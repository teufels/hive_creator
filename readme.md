![version 0.0.6](https://img.shields.io/badge/version-0.0.5-green.svg?style=flat-square)
![license MIT](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)


```
#!bash

NAME
    hive_creator

SYNOPSIS
    make project

DESCRIPTION
    The Creator clones the forked repositories.

REQUIREMENTS
    Git

    See also: https://bitbucket.org/account/user/teufels/projects/DOC.

    Using the make command requires configuration of the corresponding 'creator.ini' and existing forked repositories.

NEXTSTEPS
    cd to hive_docker from your projekt and run "make docker plain"

    See also: https://bitbucket.org/teufels/hive_docker

LISENCE:
    The MIT License (MIT)

    Copyright (c) 2016
    Andreas Hafner <a.hafner@teufels.com>,
    Dominik Hilser <d.hilser@teufels.com>,
    Georg Kathan <g.kathan@teufels.com>,
    Hendrik Krüger <h.krueger@teufels.com>,
    Perrin Ennen <p.ennen@teufels.com>,
    Timo Bittner <t.bittner@teufels.com>,
    teufels GmbH <digital@teufels.com>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.


```